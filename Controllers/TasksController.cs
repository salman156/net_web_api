using Microsoft.AspNetCore.Mvc;
using WebApi.Models;

namespace WebApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TasksController : ControllerBase
{
    private readonly TaskService taskService;
    private readonly ILogger<TasksController> _logger;

    public TasksController(ILogger<TasksController> logger, TaskService _taskService)
    {
        taskService = _taskService;
        _logger = logger;
    }

    [HttpGet]
    public IEnumerable<JiraTask> Get()
    {
        return taskService.GetTasks();
    }
    [HttpPost]
    public async void CreateAsync([FromBody] JiraTask task)
    {
        await taskService.AddTask(task);
    }

    [HttpPut("status")]
    public async void UpdateAsync([FromBody] JiraTaskStatusUpdate update_request)
    {
        JiraTask task = new JiraTask();
        task.Id = update_request.Id;
        task.Status = update_request.status;
        await taskService.UpdateTask(task);
    }

    [HttpPut("assign")]
    public async void UpdateAsync([FromBody] JiraTaskAssignUpdate update_request)
    {
        JiraTask task = new JiraTask();
        task.Id = update_request.Id;
        task.Assignee_Id = update_request.Assignee_Id;
        await taskService.UpdateTask(task);
    }
}
