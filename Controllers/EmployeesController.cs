using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using WebApi.Models;

namespace WebApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class EmployeesController : ControllerBase
{
    private readonly ILogger<EmployeesController> _logger;
    private readonly EmployeeService employeeService;
    public EmployeesController(ILogger<EmployeesController> logger, EmployeeService _employeeService)
    {
        employeeService = _employeeService;
        _logger = logger;
    }

    [HttpGet]
    public IEnumerable<Employee> GetEmployees()
    {
        return employeeService.GetEmployees();
    }
    [HttpGet("{employeeid:int}")]
    public Employee? GetEmployee(int employeeid)
    {
        return employeeService.GetEmployee(employeeid);
    }
    [HttpPost]
    public async void CreateAsync([FromBody]string fullname)
    {
        await employeeService.AddEmployee(fullname);
    }
    [HttpGet("counter")]
    public int Increment()
    {
        return employeeService.Increment();
    }
}
