using MySql.Data.MySqlClient;
using WebApi.Models;
namespace WebApi;

public class EmployeeStore : IEmployeeStore
{
    private static readonly string connectionString = "server=;user=root;database=db;port=3306;password=admin";
    public int counter = 0;
    public async Task AddAsync(string fullname)
    {
        using(var conn = GetOpenConnection())
        {
            string sql = $"INSERT INTO employee (name) values ('{fullname}');";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            await cmd.ExecuteNonQueryAsync();
        }
    }
    public int Increment()
    {
        return counter++;
    }
    private MySqlConnection GetOpenConnection()
    {
        var conn = new MySqlConnection(connectionString);
        conn.Open();
        return conn;
    }
    public IEnumerable<Employee> GetEmployees()
    {
        using(var conn = GetOpenConnection())
        {
            string sql = $"SELECT * FROM employee;";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            IList<Employee> list = new List<Employee>();
            while (rdr.Read())
            {
                list.Add(new Employee(){Id = (int)rdr[0], Name = (string)rdr[1]});
            }
            rdr.Close();
            return list;
        }
    }

    public Employee? GetEmployee(int id)
    {
        using(var conn = GetOpenConnection())
        {
            string sql = $"SELECT * FROM employee;";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            Employee? worker = null;
            while (rdr.Read())
            {
                if ((int)rdr[0] == id)
                    worker = new Employee() { Id = id, Name = (string)rdr[1]};
            }
            rdr.Close();
            return worker;
        }
    }
}