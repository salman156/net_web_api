using MySql.Data.MySqlClient;
using WebApi.Models;


namespace WebApi;
public class TaskStore
{
    private static readonly string connectionString = "server=localhost;user=root;database=db;port=3306;password=admin";
    private MySqlConnection GetOpenConnection()
    {
        var conn = new MySqlConnection(connectionString);
        conn.Open();
        return conn;
    }
    public async Task AddAsync(JiraTask task)
    {
        using(var conn = GetOpenConnection())
        {
            string sql = $"INSERT INTO task (topic, text) values ('{task.Topic}', '{task.Text}');";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            await cmd.ExecuteNonQueryAsync();
        }
    }
    public IEnumerable<JiraTask> GetAllAsync()
    {
        using(var conn = GetOpenConnection())
        {
            string sql = $"SELECT * FROM task;";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            IList<JiraTask> list = new List<JiraTask>();
            while (rdr.Read())
            {
                list.Add(new JiraTask(){Id = (int)rdr[0], 
                Topic = (string)rdr[1], 
                Text = (string)rdr[2], 
                Assignee_Id = (rdr[3] is System.DBNull)? null: (int?)rdr[3],
                Status = (JiraTaskStatus)((int)rdr[4])
                });
            }
            rdr.Close();
            return list;
        }
    }
    public async Task UpdateAsync(JiraTask task)
    {
        using(var conn = GetOpenConnection())
        {
            string sql = $"UPDATE task SET ";
            string addsql = "";
            if(task.Status != null)
                addsql += $", status_id = '{(int)task.Status}'";
            if(task.Topic != null)
                addsql += $", topic = '{task.Topic}'";
            if(task.Text != null)
                addsql += $", text = '{task.Text}'";
            if(task.Assignee_Id.HasValue)
                addsql += $", assignee_id = {task.Assignee_Id}";
            
            sql += addsql.Substring(1);
            sql += $" WHERE id = {task.Id};";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            await cmd.ExecuteNonQueryAsync();
        }
    }
}