using WebApi.Models;

namespace WebApi;

public interface IEmployeeStore
{
    Task AddAsync(string fullname);
    IEnumerable<Employee> GetEmployees();
    Employee? GetEmployee(int id);
    int Increment();
}