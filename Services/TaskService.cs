using WebApi.Models;

namespace WebApi;
public class TaskService
{
    private readonly TaskStore taskStore;
    public TaskService(TaskStore _store)
    {
        taskStore = _store;
    }
    public async Task AddTask(JiraTask task)
    {
        await taskStore.AddAsync(task);
    }
    public async Task UpdateTask(JiraTask task)
    {
        await taskStore.UpdateAsync(task);
    }
    public IEnumerable<JiraTask> GetTasks()
    {
        return taskStore.GetAllAsync();
    }
}