using WebApi.Models;

namespace WebApi;
public class EmployeeService
{
    private readonly IEmployeeStore employeeStore;
    public EmployeeService(IEmployeeStore _store)
    {
        employeeStore = _store;
    }
    public async Task AddEmployee(string fullname){
        await employeeStore.AddAsync(fullname);
    }
    public IEnumerable<Employee> GetEmployees(){
        return employeeStore.GetEmployees();
    }
    public int Increment()
    {
        return employeeStore.Increment();
    }
    public Employee? GetEmployee(int id){
        return employeeStore.GetEmployee(id);
    }
}