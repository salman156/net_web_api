namespace WebApi.Models;

public class JiraTaskStatusUpdate{
    public int Id { get; set; }

    public JiraTaskStatus status {get; set;}
}