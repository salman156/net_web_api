namespace WebApi.Models;

public class JiraTask
{
    public int Id { get; set; }
    public string? Topic { get; set; }
    public string? Text { get; set; }
    public int? Assignee_Id { get; set; }
    public JiraTaskStatus? Status { get; set; }
}