namespace WebApi.Models;

public enum JiraTaskStatus
{
    InProgress,
    Pause,
    Closed
}